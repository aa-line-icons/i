##### 450+ Free Line Icons by Ahmed Agrma

Full ownership of these icons goes to Ahmed Agrma.

- **Behance:** [behance.net/gallery/94660737/400-Free-Line-Icons](https://www.behance.net/gallery/94660737/400-Free-Line-Icons)
- **Icons list:** [aa-line-icons.gitlab.io/i/icons-list](https://aa-line-icons.gitlab.io/i/icons-list)

**How to use:**

Include this after `<head>` in your site:
```html
<link href="https://aa-line-icons.gitlab.io/i/icons.css" rel="stylesheet">
```

To place an icon, insert the following code where you want your icon to be:
```html
<i class="aa-line-icons" icon-name="light-bulb"></i>
```

Replace `light-bulb` (aka the content inside `icon-name=""`) with the name of the icon of your choosing.