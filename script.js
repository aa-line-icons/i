document.addEventListener("DOMContentLoaded", () => {
    let iconsGrid = document.querySelector(".icons-grid");
    if(iconsGrid){
        iconsGrid.querySelectorAll(".aa-line-icons[icon-name]:not([icon-name=''], .icon-single > .aa-line-icons)")?.forEach(icon => {
            let iconName = icon.getAttribute("icon-name");

            let iconSingle = document.createElement("div");
            iconSingle.classList.add("icon-single");

            let iconText = document.createElement("span");
            iconText.classList.add("icon-name");
            iconText.textContent = iconName;

            icon.before(iconSingle);
            iconSingle.append(icon);
            iconSingle.append(iconText);

            iconSingle.setAttribute("data-clipboard-text",iconName);

            let clipboard = new ClipboardJS(iconSingle);
            clipboard.on("success", () => {
                iconText.textContent = "Copied!";
                iconText.classList.add("caps");
                setTimeout(() => {
                    iconText.classList.remove("caps");
                    iconText.textContent = iconName
                },900)
            })
        })
    }
})

popify({
	button: ".credz button", // the selector to trigger the popup
	popup_selector: ".popup", // selector of the actual popup
	fade_speed: 400, // in milliseconds only
	exit_options: {
		exit_button: ".close-popup", // popup's close button
		click_outside_to_exit: true,
		escape_key_to_exit: true
	}
})
